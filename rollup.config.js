import pkg from './package.json';
import { configFactory } from '../rollup.base-config';

export default configFactory('aspectjs-core', pkg);
