# Installation

<!-- tabs:start -->
### **with npm**
```
npm install @aspectjs/core
```
### **with yarn**
```
yarn add @aspectjs/core
```
<!-- tabs:end -->


### Typescript users 
Please enable decorators support in your `tsconfig.json`:
```json
{
  "compilerOptions": {
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
    }
}
```

